# README #
This is the HackHers robotics repository for the 2015-16 FTC Season. We use Android Studio in order to make our ZTEs (Wasp and BlackWidow) able to run our robot.

## FTC 2015-16 Season: Res-Q ##

### Two Current Projects: ###
* Autonomous
* Tele-op (Driver-Controlled)

### Information ###
* Android Studio API Level 19
* [Learn Basic Android Code](https://docs.google.com/presentation/d/1WAqxxq_PECOqLLBYRHxTmBv8Qui9ou3OGmigYZjDGUc/edit?usp=sharing)
* [Learn the FTC API](https://docs.google.com/presentation/d/1eCwuHY2uxyX1Go_-hUQ6fnN0Ct9CXpb6GNeUpikk0ic/edit?usp=sharing)
* [Intelitek](http://ftc.edu.intelitek.com/)

### Requirements ###

* Download Android Studio on a personal computer, make sure you have API 19 so the ZTEs can correctly download the code
* Pull from this repo before making any changes, push any changes you make **WITH A COMMIT MESSAGE**, and make a new branch if you're creating an entirely new project.

## Contacts ##

### Sarah McLaughlin ###
* Email: smartymcsmartle@gmail.com or smm171@students.needham.k12.ma.us
* [Facebook](https://www.facebook.com/sarah.mclaughlin.12935)
* **ICE ONLY** 781-760-2412

### Gwen Marcus ###
* Email: gwentheperson@gmail.com
* [Facebook](https://www.facebook.com/profile.php?id=100010739412323)
* **ICE ONLY** 781-343-1088

### Michelle Tan ###
* Email: mtan123456789@gmail.com
* [Facebook](https://www.facebook.com/mtan123456789?fref=ts)

### Other Contacts ###
* nhsgirlsrobotics@gmail.com