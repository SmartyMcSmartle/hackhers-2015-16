package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;

/*
 * Created by Sarah McLaughlin on 11/10/15
 */
public class HHEncoderTest extends OpMode{
    DcMotor left;
    DcMotor right;

    //set and calculate numbers to use for counts
    final static int encoderCPR = 1120; //number of counts per rotation on AndyMark motors
    final static double gearRatio = (1.0 / 2); //gear ratio on robot
    final static double diameter = 3.875; //diameter of wheel (inches)
    final static int distance = 10; //distance to travel (inches)
    final static double circumference = Math.PI * diameter; //circumference of wheel (inches)
    final static double rotations = distance / circumference; //number of rotations to go distance
    final static double counts = encoderCPR + (rotations * gearRatio); //number of counts in order to go distance

    /*@Override
    public void runOpMode() throws InterruptedException{
        //get hardware map
        left = hardwareMap.dcMotor.get("left motor");
        right = hardwareMap.dcMotor.get("right motor");
        right.setDirection(DcMotor.Direction.REVERSE);

        telemetry.addData("Counts:", counts);

        left.setChannelMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
        right.setChannelMode(DcMotorController.RunMode.RUN_USING_ENCODERS);

        left.setChannelMode(DcMotorController.RunMode.RESET_ENCODERS);
        right.setChannelMode(DcMotorController.RunMode.RESET_ENCODERS);

        waitOneFullHardwareCycle();
        waitForStart();

        while(opModeIsActive()) {
            //while(left.getCurrentPosition() < counts) {
            left.setTargetPosition((int) counts);
            right.setTargetPosition((int) counts);

            left.setPower(0.5);
            right.setPower(0.5);

            left.setChannelMode(DcMotorController.RunMode.RUN_TO_POSITION);
            right.setChannelMode(DcMotorController.RunMode.RUN_TO_POSITION);

            left.setPower(0);
            right.setPower(0);

            telemetry.addData("Motor Target:", counts);
            telemetry.addData("Left Position:", left.getCurrentPosition());
            telemetry.addData("Right Position:", right.getCurrentPosition());
            //}
        }

    }*/

    final static double debug = 0.0;

    @Override
    public void init(){
        left = hardwareMap.dcMotor.get("left motor");
        right = hardwareMap.dcMotor.get("right motor");

        right.setDirection(DcMotor.Direction.REVERSE);

        left.setChannelMode(DcMotorController.RunMode.RESET_ENCODERS);
        right.setChannelMode(DcMotorController.RunMode.RESET_ENCODERS);
        telemetry.addData("Finished init: ",debug);
    }


    @Override
    public void start() {
        left.setTargetPosition(/*(int) counts*/ 100);
        right.setTargetPosition(/*(int) counts*/ 100);

        left.setChannelMode(DcMotorController.RunMode.RUN_TO_POSITION);
        right.setChannelMode(DcMotorController.RunMode.RUN_TO_POSITION);

        left.setPower(0.5);
        right.setPower(0.5);
        telemetry.addData("Finished start: ", debug);
    }

    @Override
    public void loop() {
        telemetry.addData("Motor Target:", counts);
        telemetry.addData("Left position:", left.getCurrentPosition());
        telemetry.addData("Right position:", right.getCurrentPosition());
        telemetry.addData("Finished loop: ",debug);
    }

    @Override
    public void stop() {
        left.setPower(0);
        right.setPower(0);
        telemetry.addData("Finished stop: ",debug);
    }
}
