package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
// Created by Sarah McLaughlin on 10/8/15

public class HHDriveForward extends LinearOpMode
{
    DcMotor left;
    DcMotor right;

    @Override
    public void runOpMode() throws InterruptedException
    {
        left = hardwareMap.dcMotor.get("left motor");
        right = hardwareMap.dcMotor.get("right motor");
        right.setDirection(DcMotor.Direction.REVERSE);

        left.setPower(0.5);
        right.setPower(0.5);
        sleep(2000);

        left.setPower(0);
        right.setPower(0);
    }
}
