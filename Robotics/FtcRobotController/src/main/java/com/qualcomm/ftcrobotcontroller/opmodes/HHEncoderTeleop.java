package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

/*
 * Created by Sarah McLaughlin on 10/27/15
 */
public class HHEncoderTeleop extends LinearOpMode{

    DcMotor left;
    DcMotor right;

    @Override
    public void runOpMode() throws InterruptedException{
        //get hardware map
        left = hardwareMap.dcMotor.get("left motor"); //Config: Drive Motor Controller, port 1
        //right = hardwareMap.dcMotor.get("right motor"); //Config: Drive Motor Controller, port 2
        //right.setDirection(DcMotor.Direction.REVERSE);  //right reverse for test robot, x reverse for normal robot

        double threshold = 0.1;

        waitOneFullHardwareCycle();
        waitForStart();

        while(opModeIsActive()){

            double leftY = -gamepad1.left_stick_y;
            double rightY = -gamepad1.right_stick_y;

            //use bumpers and triggers to change speed values
            if(gamepad1.left_bumper){
                left.setChannelMode(DcMotorController.RunMode.RESET_ENCODERS);
                //right.setChannelMode(DcMotorController.RunMode.RESET_ENCODERS);
            }
            else if(gamepad1.left_trigger == 1.0) {
                left.setChannelMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
                //right.setChannelMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
            }
            else if(gamepad1.right_bumper){
                left.setTargetPosition(1000);
            }

            //set sticks to power motors after threshold is reached
            if(leftY >= threshold && left.getCurrentPosition()<1000/* && right.getCurrentPosition()<1000*/){
                left.setPower(.6);
                telemetry.addData("Going till 1000:",left.getCurrentPosition());
            }
            else if(leftY >= threshold && left.getCurrentPosition()>=1000/* && right.getCurrentPosition()>=1000*/){
                left.setPower(.1);
                telemetry.addData("I'm done!:", left.getCurrentPosition());
            }
            else{
                left.setPower(0);
                telemetry.addData("Not pressing leftY:", left.getCurrentPosition());
            }


            if(rightY >=threshold) {
                left.setChannelMode(DcMotorController.RunMode.RUN_TO_POSITION);   //not sure where to set motor power
                telemetry.addData("Running to position:", left.getCurrentPosition());
            }
            else{
                left.setPower(0);
                telemetry.addData("rightY not pressed:", left.getCurrentPosition());
            }

            if(gamepad1.y)
            {
                left.setPower(.5);
                telemetry.addData("setting power to .5:", left.getCurrentPosition());
            }
            else if(gamepad1.a) {
                left.setPower(0);
                telemetry.addData("setting power to 0:",left.getCurrentPosition());
            }

            waitOneFullHardwareCycle();
        }
    }
}
