package com.qualcomm.ftcrobotcontroller.opmodes;

import android.graphics.Color;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;

/*
 * Created by Sarah McLaughlin on 11/17/15.
 */
public class HHSensorTest extends LinearOpMode {
    //declare sensor and motors
    DcMotor left;
    DcMotor right;
    ColorSensor leftDownCS;
    ColorSensor rightDownCS;
    //ColorSensor leftUpCS;
    //ColorSensor rightUpCS;

    @Override
    public void runOpMode() throws InterruptedException{
        hardwareMap.logDevices();

        //get hardware map
        left = hardwareMap.dcMotor.get("left motor");
        right = hardwareMap.dcMotor.get("right motor");
        leftDownCS = hardwareMap.colorSensor.get("left cs");
        rightDownCS = hardwareMap.colorSensor.get("right cs");
        //leftUpCS = hardwareMap.colorSensor.get("left_u_CS");
        //rightUpCS = hardwareMap.colorSensor.get("right_u_CS");

        //set left to run in reverse
        right.setDirection(DcMotor.Direction.REVERSE);   //reverse right motor for test robot, left for normal robot

        //activate color sensors
        leftDownCS.enableLed(true);
        rightDownCS.enableLed(true);
        //leftUpCS.enableLed(true);
        //rightUpCS.enableLed(true);

        //get color sensor values
        float hsvValues[] = {0F, 0F, 0F};

        // convert HSV values to RGB values
        Color.RGBToHSV(leftDownCS.red(), leftDownCS.green(), leftDownCS.blue(), hsvValues);
        Color.RGBToHSV(rightDownCS.red(), rightDownCS.green(), rightDownCS.blue(), hsvValues);
        //Color.RGBToHSV(leftUpCS.red(), leftUpCS.green(), leftUpCS.blue(), hsvValues);
        //Color.RGBToHSV(rightUpCS.red(), rightUpCS.green(), rightUpCS.blue(), hsvValues);

        //start program


        //create threshold
            //set left color sensor values to variables
            int red1 = leftDownCS.red();
            int blu1 = leftDownCS.blue();
            int grn1 = leftDownCS.green();

            //set right color sensor values to variables
            int red2 = rightDownCS.red();
            int blu2 = rightDownCS.blue();
            int grn2 = rightDownCS.green();

            //set threshold from the highest start value + 5
            int threshold = 5 + Math.max(Math.max(grn1, grn2), Math.max(Math.max(red1, red2), Math.max(blu1, blu2)));

        //wait for start button to be pushed
        waitOneFullHardwareCycle();
        waitForStart();

        //drive forward until a light sensor sees any color
        while(true){
            //add data to screen for debugging
            red1 = leftDownCS.red();
            blu1 = leftDownCS.blue();
            grn1 = leftDownCS.green();

            //set right color sensor values to variables
            red2 = rightDownCS.red();
            blu2 = rightDownCS.blue();
            grn2 = rightDownCS.green();

            if(grn1 < threshold && red1 < threshold && blu1 < threshold && grn2 < threshold && red2 < threshold && blu2 < threshold) {
                forward(0.3);
            }

            else {
                break;
            }
        }
        stopMotors();
        sleep(2000);
    }


    public void forward(double speed){
        left.setPower(speed);
        right.setPower(speed);
    }

    public void backward(double speed){
        left.setPower(-speed);
        right.setPower(-speed);
    }

    public void stopMotors(){
        left.setPower(0);
        right.setPower(0);
    }

    public void turnLeft(double speed){
        left.setPower(0);
        right.setPower(speed);
    }

    public void turnRight(double speed){
        left.setPower(speed);
        right.setPower(0);
    }

    public void spinClockwise(double speed){
        left.setPower(speed);
        right.setPower(-speed);
    }

    public void spinCounterclockwise(double speed){
        left.setPower(-speed);
        right.setPower(speed);
    }
}
