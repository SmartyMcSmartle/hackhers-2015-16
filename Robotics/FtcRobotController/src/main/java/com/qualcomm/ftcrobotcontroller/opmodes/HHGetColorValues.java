package com.qualcomm.ftcrobotcontroller.opmodes;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;

import com.qualcomm.ftcrobotcontroller.R;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;


/*
 * Created by Gwen Marcus on 27/10/15.
 */
public class HHGetColorValues extends LinearOpMode {

    ColorSensor leftCS;
    ColorSensor rightCS;

    @Override
    public void runOpMode() throws InterruptedException {
        hardwareMap.logDevices();

        // get a reference to our ColorSensor object.
        leftCS = hardwareMap.colorSensor.get("left cs");
        rightCS = hardwareMap.colorSensor.get("right cs");

        // turn the LED on in the beginning, just so user will know that the sensor is active.
        leftCS.enableLed(true);
        rightCS.enableLed(true);

        // wait one cycle and for start button to be pressed
        waitOneFullHardwareCycle();
        waitForStart();

        // hsvValues is an array that will hold the hue, saturation, and value information.
        float hsvValues[] = {0F,0F,0F};

        // values is a reference to the hsvValues array.
        final float values[] = hsvValues;

        // get a reference to the RelativeLayout so we can change the background
        // color of the Robot Controller app to match the hue detected by the RGB sensor.
        final View relativeLayout = ((Activity) hardwareMap.appContext).findViewById(R.id.RelativeLayout);

        while (opModeIsActive()) {
            // convert the RGB values to HSV values.
            Color.RGBToHSV(leftCS.red(), leftCS.green(), leftCS.blue(), hsvValues);
            Color.RGBToHSV(rightCS.red(), rightCS.green(), rightCS.blue(), hsvValues);

            int red1 = leftCS.red();
            int blu1 = leftCS.blue();
            int grn1 = leftCS.green();

            int red2 = rightCS.red();
            int blu2 = rightCS.blue();
            int grn2 = rightCS.green();

            // change the background color to match the color detected by the RGB sensor.
            // pass a reference to the hue, saturation, and value array as an argument
            // to the HSVToColor method.
            telemetry.addData("Red Left:", red1);
            telemetry.addData("Green Left:", grn1);
            telemetry.addData("Blue Left:", blu1);
            telemetry.addData("Red Right:", red2);
            telemetry.addData("Green Right:", grn2);
            telemetry.addData("Blue Right:", blu2);
            relativeLayout.post(new Runnable() {
                public void run() {
                    relativeLayout.setBackgroundColor(Color.HSVToColor(0xff, values));
                }
            });

            // wait a hardware cycle before iterating.
            waitOneFullHardwareCycle();
        }

    }
}
