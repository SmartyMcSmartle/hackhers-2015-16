package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

/*
 * Created by Michelle Tan on 11/28/15.
 */
public class HHWheelTest extends LinearOpMode{
    DcMotor left;
    DcMotor right;

    @Override
    public void runOpMode() throws InterruptedException
    {
        double power = 0.3;

        left = hardwareMap.dcMotor.get("left motor");
        right = hardwareMap.dcMotor.get("right motor");
        right.setDirection(DcMotor.Direction.REVERSE);

        waitOneFullHardwareCycle();
        waitForStart();

        left.setPower(power);       //forwards
        right.setPower(power);
        sleep(500);

        left.setPower(0);            //stop
        right.setPower(0);
        sleep(200);

        left.setPower(-power);         //backwards
        right.setPower(-power);
        sleep(500);

        left.setPower(0);           //stop
        right.setPower(0);
        sleep(200);

        left.setPower(-power);       //left
        right.setPower(power);
        sleep(500);

        left.setPower(0);       //stop
        right.setPower(0);
        sleep(200);

        left.setPower(power);       //right
        right.setPower(-power);
        sleep(500);

        left.setPower(0);       //stop
        right.setPower(0);
    }
}
