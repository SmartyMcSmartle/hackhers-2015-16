package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;

/*
 * Created by Sarah Mclaughlin on 1/5/16.
 */
public class HHEncoderTest3 extends OpMode {
    DcMotor test;
    public void init() {
        test = hardwareMap.dcMotor.get("test");
        test.setPower(0.5);
    }
    public void start() {

    }
    public void loop() {
        if(gamepad1.y) {
            test.setChannelMode(DcMotorController.RunMode.RUN_TO_POSITION);
            test.setTargetPosition(1120);
        }
        else {
            test.setChannelMode(DcMotorController.RunMode.RUN_TO_POSITION);
            test.setTargetPosition(100);
        }
    }
}
