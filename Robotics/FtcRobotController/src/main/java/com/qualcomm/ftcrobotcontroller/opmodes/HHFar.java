package com.qualcomm.ftcrobotcontroller.opmodes;

import android.graphics.Color;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * Created by sonja on 11/12/2015.
 */
public class HHFar extends LinearOpMode {
    //declare sensor and motors
    DcMotor left;
    DcMotor right;
    ColorSensor leftDownCS;
    ColorSensor rightDownCS;
    //ColorSensor leftUpCS;
    //ColorSensor rightUpCS;

    @Override
    public void runOpMode() throws InterruptedException{
        //Movement Chimera = new Movement(1, 0.5, left, right);

        //get hardware map
        left = hardwareMap.dcMotor.get("left_motor");
        right = hardwareMap.dcMotor.get("right_motor");
        leftDownCS = hardwareMap.colorSensor.get("left_d_CS");
        rightDownCS = hardwareMap.colorSensor.get("right_d_CS");
        //leftUpCS = hardwareMap.colorSensor.get("left_u_CS");
        //rightUpCS = hardwareMap.colorSensor.get("right_u_CS");

        //activate color sensors
        leftDownCS.enableLed(true);
        rightDownCS.enableLed(true);
        //leftUpCS.enableLed(true);
        //rightUpCS.enableLed(true);

        //wait for start button to be pushed
        waitOneFullHardwareCycle();
        waitForStart();

        //get color sensor values
        float hsvValues[] = {0F,0F,0F};
        int threshold = 40;

        while(opModeIsActive()){
            // convert RGB values to HSV values
            Color.RGBToHSV(leftDownCS.red(), leftDownCS.green(), leftDownCS.blue(), hsvValues);
            Color.RGBToHSV(rightDownCS.red(), rightDownCS.green(), rightDownCS.blue(), hsvValues);
            //Color.RGBToHSV(leftUpCS.red(), leftUpCS.green(), leftUpCS.blue(), hsvValues);
            //Color.RGBToHSV(rightUpCS.red(), rightUpCS.green(), rightUpCS.blue(), hsvValues);

            //*******start program**********

            //set left color sensor values to variables
            int red1 = leftDownCS.red();
            int blu1 = leftDownCS.blue();
            int grn1 = leftDownCS.green();

            //set right color sensor values to variables
            int red2 = rightDownCS.red();
            int blu2 = rightDownCS.blue();
            int grn2 = rightDownCS.green();

            int direction = 0;

            //drive forward until a light sensor sees any color
            while(grn1 < threshold || red1 < threshold || blu1 < threshold || grn2 < threshold || red2 < threshold || blu2 < threshold){
                //Chimera.forward();
            }

            //Chimera.stop();
            sleep(200);

            //creates empty color variables for the color to look for during match
            int firstColorS1 = 0;
            int firstColorS2 = 0;

            //sets empty color variable to a specific color to look for during match
            if(grn1 > threshold || grn2 > threshold){
                firstColorS1 = grn1;
                firstColorS2 = grn2;
            }
            else if(blu1 > threshold || blu2 > threshold){
                firstColorS1 = blu1;
                firstColorS2 = blu2;
            }
            else if(red1 > threshold || red2 > threshold){
                firstColorS1 = red1;
                firstColorS2 = red2;
            }

            /*
            Sets the robot so if color sensors see one color, they will go one direction, and if they see
            another color they'll go the other way.
             */
            if(firstColorS1 == blu1 || firstColorS1 == grn1){
                direction = 1;
            }
            else{
                direction = 2;
            }

            /*
            rotate until both color sensors see [color]
            rotate 90 degrees [direction]
            drive forwards [distance]
            rotate [direction] until both color sensors touch [color]
            drive forwards 6 inches and keep driving until color sensor senses [color]
            rotate until other wheel touches [color]

            */
        }
    }
}
