package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;

/*
 * Created by Sarah McLaughlin on 12/27/15
 */
public class Movement {
    private double speed;
    private DcMotor motorL;
    private DcMotor motorR;

    public Movement(DcMotor l, DcMotor r) {
        motorL = l;
        motorR = r;
    }

    //basic movements
    public void forward(double s) {
        speed = s;
        motorL.setPower(speed);
        motorR.setPower(speed);
    }
    public void backward(double s) {
        speed = s;
        motorL.setPower(-speed);
        motorR.setPower(-speed);
    }
    public void clockwise(double s) {
        speed = s;
        motorL.setPower(speed);
        motorR.setPower(-speed);
    }
    public void counterClockwise(double s) {
        speed = s;
        motorL.setPower(-speed);
        motorR.setPower(speed);
    }
    public void turnRight(double s) {
        speed = s;
        motorL.setPower(speed);
        motorR.setPower(0);
    }
    public void turnLeft(double s) {
        speed = s;
        motorL.setPower(0);
        motorR.setPower(speed);
    }
    public void stop() {
        motorL.setPower(0);
        motorR.setPower(0);
    }
}
