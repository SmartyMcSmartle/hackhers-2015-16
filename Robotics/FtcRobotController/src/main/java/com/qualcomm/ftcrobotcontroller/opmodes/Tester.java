package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

public class Tester extends LinearOpMode {
    DcMotor left;
    DcMotor right;

    @Override
    public void runOpMode() throws InterruptedException {
        //moves forward for 3 seconds
        gamepad1.right_stick_y = (float) 0.7;

        while(gamepad1.right_trigger == 1) {
            right.setPower(1);
            left.setPower(1);
            sleep(3000);
        }

        //moves backward for 5 seconds
        right.setPower(-1);
        left.setPower(-1);
        sleep(5000);

        //rotates to the left for 5 seconds
        right.setPower(1);
        left.setPower(-1);
        sleep(5000);

        //rotates to the right for 5 seconds
        right.setPower(-1);
        left.setPower(1);
        sleep(5000);

        //turns to the right for 5 seconds
        right.setPower(0);
        left.setPower(1);
        sleep(5000);
    }
}