package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DeviceInterfaceModule;

/*
 * Created by Autonomous Team on 10/6/16
 */

public class HH2016BallAutoRedLeft extends LinearOpMode{

    DcMotor leftfront;
    DcMotor rightfront;
    DcMotor leftback;
    DcMotor rightback;

    ColorSensor rightcolor;
    //ColorSensor leftcolor;
    //ColorSensor topcolor;

    DeviceInterfaceModule cdim;

    public void runOpMode() throws InterruptedException{
        hardwareMap.logDevices();

        leftfront = hardwareMap.dcMotor.get("leftfront");
        rightfront = hardwareMap.dcMotor.get("rightfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightback = hardwareMap.dcMotor.get("rightback");
        rightcolor = hardwareMap.colorSensor.get("rightcolor");
        //leftcolor = hardwareMap.colorSensor.get("leftcolor");
        //topcolor = hardwareMap.colorSensor.get("topcolor");
        //rightfront.setDirection(DcMotor.Direction.REVERSE);
        //rightback.setDirection(DcMotor.Direction.REVERSE);

        cdim = hardwareMap.deviceInterfaceModule.get("dim");

        waitOneFullHardwareCycle();
        waitForStart();

        double speed = 0.4;

        while(/*color sensor doesn't see red*/speed > 0.5){
            driveAll(speed);
        }
        driveAll(0);
        while(/*color sensor sees red*/speed > 0.5){

        }

    }

    public void driveAll(double speed){
        leftfront.setPower(speed);
        leftback.setPower(speed);
        rightfront.setPower(speed);
        rightback.setPower(speed);
    }

    public void pivot(double speed){  //positive speed turns cw, negative speed turns ccw
        leftfront.setPower(speed);
        leftback.setPower(speed);
        rightfront.setPower(-speed);
        rightback.setPower(-speed);
    }

    public void turn(double speed, boolean left){
        if(left){
            leftfront.setPower(speed);
            leftback.setPower(speed);
            rightfront.setPower(0);
            rightback.setPower(0);

        } else {
            rightfront.setPower(speed);
            rightback.setPower(speed);
            leftfront.setPower(0);
            leftback.setPower(0);
        }
    }

}
