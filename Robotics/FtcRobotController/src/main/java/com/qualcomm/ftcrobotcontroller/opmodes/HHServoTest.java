package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

/*
 * Created by Sarah Mclaughlin on 12/15/15.
 */
public class HHServoTest extends LinearOpMode{
    DcMotor left;
    DcMotor right;
    Servo flickR;
    Servo flickL;
    Servo zipline;
    final double POSITIONL = 0.0;
    final double POSITIONR = 0.0;

    public void runOpMode() throws InterruptedException {

        left = hardwareMap.dcMotor.get("left motor");
        right = hardwareMap.dcMotor.get("right motor");
        flickL = hardwareMap.servo.get("flickl");
        flickR = hardwareMap.servo.get("flickr");
        zipline = hardwareMap.servo.get("zipline");
        right.setDirection(DcMotor.Direction.REVERSE);
        waitOneFullHardwareCycle();
        waitForStart();
        flickL.setPosition(POSITIONL);
        flickR.setPosition(POSITIONR);
    }
}
