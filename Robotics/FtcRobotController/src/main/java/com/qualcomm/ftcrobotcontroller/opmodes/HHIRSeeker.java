package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.ftccommon.DbgLog;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.IrSeekerSensor;

/*
 * Created by Sarah McLaughlin on 4/14/16
 * Ready to test
 */
public class HHIRSeeker extends LinearOpMode{

    IrSeekerSensor irSensor;
    DcMotor left;
    DcMotor right;

    final static double motor_power = 0.5;
    final static double sig_strength = 0.2;

    public void runOpMode(){
        irSensor = hardwareMap.irSeekerSensor.get("ir_Sensor");
        left = hardwareMap.dcMotor.get("left");
        right = hardwareMap.dcMotor.get("right");
        
        //set motor to reverse direction if needed in this line of code
        
        while(opModeIsActive()){
            double angle = 0;
            double strength = 0;

            // Is an IR signal detected?
            if (irSensor.signalDetected()) {
                // an IR signal is detected

                // Get the angle and strength of the signal
                angle = irSensor.getAngle();
                strength = irSensor.getStrength();

                // which direction should we move?
                if (angle < -20) {
                    // we need to move to the left
                    right.setPower(motor_power);
                    left.setPower(-motor_power);
                } else if (angle > 20) {
                    // we need to move to the right
                    right.setPower(-motor_power);
                    left.setPower(motor_power);
                } else if (strength < sig_strength) {
                    // the IR signal is weak, approach
                    right.setPower(motor_power);
                    left.setPower(motor_power);
                } else {
                    // the IR signal is strong, stay here
                    right.setPower(0.0);
                    left.setPower(0.0);
                }
            } else {
                // no IR signal is detected
                right.setPower(0.0);
                left.setPower(0.0);
            }

            telemetry.addData("angle", angle);
            telemetry.addData("strength", strength);

            DbgLog.msg(irSensor.toString());
        }
    }
}
