package com.qualcomm.ftcrobotcontroller.opmodes;

import android.graphics.Color;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorController;

/*
 * Created by Sarah McLaughlin on 11/17/15.
 */
public class HHEncoderTest2 extends LinearOpMode {
    //declare sensor and motors
    DcMotor left;
    DcMotor right;
    ColorSensor leftDownCS;
    ColorSensor rightDownCS;
    //ColorSensor leftUpCS;
    //ColorSensor rightUpCS;

    @Override
    public void runOpMode() throws InterruptedException{
        int one = 1;
        int two = 2;
        int three = 3;
        int four = 4;
        int five = 5;
        int six = 6;
        int seven = 7;

        hardwareMap.logDevices();

        //get hardware map
        left = hardwareMap.dcMotor.get("left motor");
        right = hardwareMap.dcMotor.get("right motor");
        //leftDownCS = hardwareMap.colorSensor.get("left cs");
        //rightDownCS = hardwareMap.colorSensor.get("right cs");
        //leftUpCS = hardwareMap.colorSensor.get("left_u_CS");
        //rightUpCS = hardwareMap.colorSensor.get("right_u_CS");
        right.setDirection(DcMotor.Direction.REVERSE);
        telemetry.addData("Finish hardwareMap", one);
        sleep(2000);


        left.setChannelMode(DcMotorController.RunMode.RESET_ENCODERS);
        right.setChannelMode(DcMotorController.RunMode.RESET_ENCODERS);
        telemetry.addData("Finish encoder reset", three);
        sleep(2000);

        left.setTargetPosition(1120);
        right.setTargetPosition(1120);
        telemetry.addData("Finish set target position", four);
        sleep(2000);

        waitForStart();

        telemetry.addData("Left position:", left.getCurrentPosition());
        telemetry.addData("Right position:", right.getCurrentPosition());
        sleep(1000);

        left.setChannelMode(DcMotorController.RunMode.RUN_TO_POSITION);
        right.setChannelMode(DcMotorController.RunMode.RUN_TO_POSITION);

        telemetry.addData("Left position2:", left.getCurrentPosition());
        telemetry.addData("Right position2:", right.getCurrentPosition());

        telemetry.addData("finish run to position", five);
        sleep(2000);

        left.setChannelMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
        right.setChannelMode(DcMotorController.RunMode.RUN_USING_ENCODERS);
        telemetry.addData("Finish Run w/ Encoders", two);
        sleep(2000);


        left.setPower(.5);
        right.setPower(.5);
        telemetry.addData("Finish set power", six);

        left.setPower(0);
        right.setPower(0);
        telemetry.addData("Stop robot", seven);
    }



}