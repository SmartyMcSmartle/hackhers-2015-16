package com.qualcomm.ftcrobotcontroller.opmodes;

import android.graphics.Color;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.LightSensor;

/*
 * Created by Audrey Pratt on 10/22/15.
 */

public class HHNearBlue extends LinearOpMode{
    DcMotor left;
    DcMotor right;
    DcMotor arm;
    DcMotor rope;

    Servo flickR;
    Servo flickL;
    Servo zipLine;

    ColorSensor leftCS;
    ColorSensor rightCS;

    LightSensor lS;
   public void runOpMode() throws InterruptedException {
       //get hardware map
       left = hardwareMap.dcMotor.get("left motor"); //Config: Drive Motor Controller, port 1
       right = hardwareMap.dcMotor.get("right motor"); //Config: Drive Motor Controller, port 2
       arm = hardwareMap.dcMotor.get("arm"); //Config: Hang Motor Controller, port 1
       rope = hardwareMap.dcMotor.get("rope"); //Config: Hang Motor Controller, port 2

       flickR = hardwareMap.servo.get("flick R"); //Config: Servo Controller, channel 1
       flickL = hardwareMap.servo.get("flick L"); //Config: Servo Controller, channel 2
       zipLine = hardwareMap.servo.get("zip line"); //Config: Servo Controller, channel 3

       leftCS = hardwareMap.colorSensor.get("left CS");
       rightCS = hardwareMap.colorSensor.get("right CS");

       lS = hardwareMap.lightSensor.get("light");

       //set one motor to reversed state
       right.setDirection(DcMotor.Direction.REVERSE);  //right reverse for test robot, x reverse for normal robot

       //activate sensors
       leftCS.enableLed(true);
       rightCS.enableLed(true);
       lS.enableLed(true);

       //get color sensor values
       float hsvValues[] = {0F,0F,0F};

       // convert RGB values to HSV values
       Color.RGBToHSV(leftCS.red(), leftCS.green(), leftCS.blue(), hsvValues);
       Color.RGBToHSV(rightCS.red(), rightCS.green(), rightCS.blue(), hsvValues);

       //start program

       //set left color sensor values to variables
       int redL = leftCS.red();
       int bluL = leftCS.blue();
       int grnL = leftCS.green();

       //set right color sensor values to variables
       int redR = rightCS.red();
       int bluR = rightCS.blue();
       int grnR = rightCS.green();

       //set light sensor value to variable
       double lite = lS.getLightDetected();

       //create threshold for color sensor
       int threshold = 5 + Math.max(Math.max(grnL, grnR), Math.max(Math.max(redL, redR), Math.max(bluL, bluR)));

       //set the value of white
       double white = 0.15 + lS.getLightDetected(); //TEST THIS VALUE!!!!!!!!

       //create an object to represent our robot
       Movement chimera = new Movement(left, right);

       waitOneFullHardwareCycle();
       waitForStart();

       //going forward until color sensor sees something
       while(true) {
           //update color sensor values
           redL = leftCS.red();
           bluL = leftCS.blue();
           grnL = leftCS.green();
           redR = rightCS.red();
           bluR = rightCS.blue();
           grnR = rightCS.green();

           if(grnL < threshold && redL < threshold && bluL < threshold && grnR < threshold && redR < threshold && bluR < threshold) {
               chimera.forward(0.3);
           } else {
               break;
           }
       }
       chimera.stop();
       sleep(2000);

       //turn till light sensor sees white line
       while(true) {
           //update light sensor value
           lite = lS.getLightDetected();

           if(lite < white) {
               chimera.turnRight(0.3);
           } else {
               break;
           }
       }
       chimera.stop();
       sleep(2000);

       //follow the white line
       while(true /* insert timer or encoder or touch sensor code here*/) {
           lite = lS.getLightDetected();

           if(lite > white) {
               //if the robot sees white, have it drift to the left
               left.setPower(0.5);
               right.setPower(0.55);
           } else if(lite < white) {
               //if the robot doesn't see white, have it drift at a sharper angle right
               left.setPower(0.55);
               right.setPower(0.45);
           } else { //delete else loop once timer or touch sensor code is inserted
               break;
           }
       }
       chimera.stop();
       sleep(2000);

       //dump climebrs
       zipLine.setPosition(1); //need to test positioning of the servo, DO THIS IN TELEOP FIRST
       zipLine.setPosition(0);

       //once the program is done, delete or (if need be) decrease sleep

       /* Go forward until we hit the blue tape -- WRITTEN, NEEDS TESTING
        * Turn to the right until the light sensor sees the white line -- WRITTEN, NEEDS TESTING
        * Go forward following the white tape until we hit wall -- NEED TO DECIDE ON TOUCH SENSORS OR TIMER, NEEDS TESTING
        * Dump climbers into shelter -- WRITTEN, NEED SPECIFIC VALUES, NEED TESTING
        * Hit beacon ??? (hardware is nonexistent) -- NOTHING TO WRITE AS OF NOW
        */
   }
}
