package com.qualcomm.ftcrobotcontroller.opmodes;

        import android.graphics.Color;

        import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
        import com.qualcomm.robotcore.hardware.TouchSensor;
        import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * Created by mtan on 2/9/16. Michelle and Suela learn about touch sensors
 */
public class HHTouchSensor extends LinearOpMode {
    DcMotor left;
    DcMotor right;
    TouchSensor touch;


    @Override
    public void runOpMode() throws InterruptedException {
        //get references to the motors from the hardware map
        left = hardwareMap.dcMotor.get("left motor");
        right = hardwareMap.dcMotor.get("right motor");

        //reverse the right motor
        right.setDirection(DcMotor.Direction.REVERSE);

        //get a reference to the touch sensor
        touch = hardwareMap.touchSensor.get("sensor_touch");

        if (touch.isPressed()) {
            //Stop the motors if the touch sensor is pressed
            left.setPower(0);
            right.setPower(0);
        } else {
            //keep moving if touch sensor is not pressed
            left.setPower(0.5);
            right.setPower(0.5);
        }
        telemetry.addData("ispressed", String.valueOf(touch.isPressed()));
    }
}