package com.qualcomm.ftcrobotcontroller.opmodes;

import android.graphics.Color;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;

/*
 * Created by Audrey Pratt on 10/22/15.
 */


public class HHNear extends LinearOpMode{
    //declare sensor and motors
    DcMotor left;
    DcMotor right;
    ColorSensor leftDownCS;
    ColorSensor rightDownCS;
    //ColorSensor leftUpCS;
    //ColorSensor rightUpCS;

    @Override
    public void runOpMode() throws InterruptedException{
        //get hardware map
        left = hardwareMap.dcMotor.get("left motor");
        right = hardwareMap.dcMotor.get("right motor");
        leftDownCS = hardwareMap.colorSensor.get("left cs");
        rightDownCS = hardwareMap.colorSensor.get("right cs");
        //leftUpCS = hardwareMap.colorSensor.get("left_u_CS");
        //rightUpCS = hardwareMap.colorSensor.get("right_u_CS");

        //set left to run in reverse
        left.setDirection(DcMotor.Direction.REVERSE);

        //activate color sensors
        leftDownCS.enableLed(true);
        rightDownCS.enableLed(true);
        //leftUpCS.enableLed(true);
        //rightUpCS.enableLed(true);

        //wait for start button to be pushed
        waitOneFullHardwareCycle();
        waitForStart();

        //get color sensor values
        float hsvValues[] = {0F,0F,0F};
        int threshold = 12;

        while(opModeIsActive()){
            // convert RGB values to HSV values
            Color.RGBToHSV(leftDownCS.red(), leftDownCS.green(), leftDownCS.blue(), hsvValues);
            Color.RGBToHSV(rightDownCS.red(), rightDownCS.green(), rightDownCS.blue(), hsvValues);
            //Color.RGBToHSV(leftUpCS.red(), leftUpCS.green(), leftUpCS.blue(), hsvValues);
            //Color.RGBToHSV(rightUpCS.red(), rightUpCS.green(), rightUpCS.blue(), hsvValues);

            //start program

            //set left color sensor values to variables
            int red1 = leftDownCS.red();
            int blu1 = leftDownCS.blue();
            int grn1 = leftDownCS.green();

            //set right color sensor values to variables
            int red2 = rightDownCS.red();
            int blu2 = rightDownCS.blue();
            int grn2 = rightDownCS.green();

            //drive forward until a light sensor sees any color
            while(true){
                if(grn1 < threshold && red1 < threshold && blu1 < threshold && grn2 < threshold && red2 < threshold && blu2 < threshold){
                    forward(0.3);
                }
                else{
                    stopMotors();
                    sleep(2000);
                    break;
                }
            }

            //creates empty color variables for the color to look for during match
            int color1 = 0;
            int color2 = 0;

            //sets empty color variable to a specific color to look for during match
            if(grn1 > threshold || grn2 > threshold) {
                color1 = grn1;
                color2 = grn2;
            }
            else if(blu1 > threshold || blu2 > threshold) {
                color1 = blu1;
                color2 = blu2;
            }
            else if(red1 > threshold || red2 > threshold) {
                color1 = red1;
                color2 = red2;
            }

            //Robot turns right if color sees blue, but if it sees red turns left
            while (true) {
                if(color2 < threshold) {
                    turnRight(0.3);
                }
                else if(color1 < threshold) {
                    turnLeft(0.3);
                }
                else {
                    stopMotors();
                    break;
                }
            }

            stopMotors();
            sleep(200);

            /*
            if(color1 == blu1){
                turnRight();
                sleep(2000);
            }
            else if(color1 == red1){
                turnLeft();
                sleep(2000);

            }

            forward();
            sleep(2000);

            if(color1 == blu1) {
                forward();
                sleep();
            }
            else if(color1 != blu1){
                backward();
            }*/

            /*
            turn so that it faces correct blue slope
            check that slope is blue slope using color sensor
            if not blue, back up, turn, go left for short time, check again
            if blue, turn so that the front of the robot is parallel with slop
            go up slope after raising power a bit to account for angle of slope
            stop after robot reaches top
            */
        }
    }


    public void forward(double speed){
        left.setPower(speed);
        right.setPower(speed);
    }

    public void backward(double speed){
        left.setPower(-speed);
        right.setPower(-speed);
    }

    public void stopMotors(){
        left.setPower(0);
        right.setPower(0);
    }

    public void turnLeft(double speed){
        left.setPower(0);
        right.setPower(speed);
    }

    public void turnRight(double speed){
        left.setPower(speed);
        right.setPower(0);
    }

    public void spinClockwise(double speed){
        left.setPower(speed);
        right.setPower(-speed);
    }

    public void spinCounterclockwise(double speed){
        left.setPower(-speed);
        right.setPower(speed);
    }
}
