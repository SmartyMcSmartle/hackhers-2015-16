package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

/*
 * Created by Sarah and Annalise on 9/20/16
 */

public class HHTeleop2016 extends LinearOpMode{

    //declaring and naming motor variables
    DcMotor leftfront;
    DcMotor leftback;
    DcMotor rightfront;
    DcMotor rightback;

    public void runOpMode() throws InterruptedException {
        //configure hardware map & reverse right motor directions
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightfront = hardwareMap.dcMotor.get("rightfront");
        rightback = hardwareMap.dcMotor.get("rightback");
        rightfront.setDirection(DcMotor.Direction.REVERSE);
        rightback.setDirection(DcMotor.Direction.REVERSE);

        double threshold = 0.2;
        double scale = 0.5;

        waitOneFullHardwareCycle();
        waitForStart();

        while(opModeIsActive()) {
            //make leftY and rightY variables linked to gamepad vals
            double leftY = -gamepad1.left_stick_y;
            double rightY = -gamepad1.right_stick_y;

            //make speed variables for both wheels
            double speedleft = scale * Math.abs(leftY);
            double speedright = scale * Math.abs(rightY);

            //display speed values
            telemetry.addData("Left Power", speedleft);
            telemetry.addData("Right Power", speedright);

            //code to control the robot
            if (leftY >= threshold) {
                leftfront.setPower(speedleft);
                leftback.setPower(speedleft);
            } else if (leftY <= -threshold) {
                leftfront.setPower(-speedleft);
                leftback.setPower(-speedleft);
            } else {
                leftfront.setPower(0);
                leftback.setPower(0);
            }
            if (rightY >= threshold) {
                rightfront.setPower(speedright);
                rightback.setPower(speedright);
            } else if (rightY <= -threshold) {
                rightfront.setPower(-speedright);
                rightback.setPower(-speedright);
            } else {
                rightfront.setPower(0);
                rightback.setPower(0);
            }

            waitOneFullHardwareCycle();
        }
    }
}
