package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;

/*
 * Created by Sarah McLaughlin on 10/27/15
 */
public class HHTeleop1 extends LinearOpMode{

    DcMotor left;
    DcMotor right;
    DcMotor arm;
    DcMotor rope;

    Servo flickR;
    Servo flickL;
    Servo zipLine;

    @Override
    public void runOpMode() throws InterruptedException{
        //get hardware map
        left = hardwareMap.dcMotor.get("left motor"); //Config: Drive Motor Controller, port 1
        right = hardwareMap.dcMotor.get("right motor"); //Config: Drive Motor Controller, port 2
        arm = hardwareMap.dcMotor.get("arm"); //Config: Hang Motor Controller, port 1
        rope = hardwareMap.dcMotor.get("rope"); //Config: Hang Motor Controller, port 2
        flickR = hardwareMap.servo.get("flick R"); //Config: Servo Controller, channel 1
        flickL = hardwareMap.servo.get("flick L"); //Config: Servo Controller, channel 2
        zipLine = hardwareMap.servo.get("zip line"); //Config: Servo Controller, channel 3
        right.setDirection(DcMotor.Direction.REVERSE);  //right reverse for test robot, x reverse for normal robot

        //set threshold values and starting speed
        double threshold = 0.1;
        double speed = 0.5;
        double armSpeed = 0.0;
        //double zipPosition = 0.6;

        flickL.setPosition(0);
        flickR.setPosition(0.9);
        zipLine.setPosition(1.0);

        waitOneFullHardwareCycle();
        waitForStart();

        while(opModeIsActive()){
            //set speeds so they can't be less than zero or greater than 1
            speed = Range.clip(speed, 0.01, 0.99);
            //zipPosition = Range.clip(zipPosition, 0.01, 0.99);

            //set variable equal to the joystick value
            double leftY = -gamepad1.left_stick_y;
            double rightY = -gamepad1.right_stick_y;

            //displays the power of each motor
            telemetry.addData("Left Motor Power", speed);
            telemetry.addData("Right Motor Power", speed);
            //telemetry.addData("Zip Line Position", zipPosition);

            //use bumpers and triggers to change speed values
            if(gamepad1.left_bumper){
                speed += 0.05;
            } else if(gamepad1.left_trigger == 1.0){
                speed -= 0.05;
            }
            if(gamepad1.right_bumper){
                speed += 0.0075;
            } else if(gamepad1.right_trigger == 1.0){
                speed -= 0.0075;
            }

            //set sticks to power motors after threshold is reached
            if(leftY >= threshold){
                left.setPower(speed);
            } else if(leftY <= -threshold){
                left.setPower(-speed);
            } else {
                left.setPower(0);
            }
            if(rightY >= threshold){
                right.setPower(speed);
            } else if(rightY <= -threshold){
                right.setPower(-speed);
            } else {
                right.setPower(0);
            }


            if(gamepad2.left_bumper){
                flickL.setPosition(0);              //up is 0
            } else if(gamepad2.left_trigger == 1.0){
                flickL.setPosition(0.7);
            }
            if(gamepad2.right_bumper) {
                flickR.setPosition(0.9);
            } else if(gamepad2.right_trigger == 1.0){
                flickR.setPosition(0.4);
            }

            if(gamepad2.dpad_up) {
                zipLine.setPosition(0.0);
            } else if(gamepad2.dpad_down){
                zipLine.setPosition(1.0);
            }


            if(gamepad2.y) {
                arm.setPower(armSpeed);
            } else if(gamepad2.a) {
                arm.setPower(-armSpeed);
            } else {
                arm.setPower(0);
            }

            if(gamepad2.x) {
                armSpeed = 0.8;
            } else if(gamepad2.b) {
                armSpeed = 1;
            }

            if(gamepad2.left_stick_y > threshold) {
                rope.setPower(-1);
            } else if(gamepad2.left_stick_y < -threshold){
                rope.setPower(1);
            }else {
                rope.setPower(0);
            }

            //zipLine.setPosition(zipPosition);

            waitOneFullHardwareCycle();
        }
    }
}
