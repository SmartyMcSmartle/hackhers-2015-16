package com.qualcomm.ftcrobotcontroller.opmodes;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;

/*
 * Created by Grace and Sarah on 9/22/16
 */
public class HHAutonomousTest2016 extends LinearOpMode {

    DcMotor leftfront;
    DcMotor leftback;
    DcMotor rightfront;
    DcMotor rightback;

    public void runOpMode() throws InterruptedException {
        leftfront = hardwareMap.dcMotor.get("leftfront");
        leftback = hardwareMap.dcMotor.get("leftback");
        rightfront = hardwareMap.dcMotor.get("rightfront");
        rightback = hardwareMap.dcMotor.get("rightback");
        rightfront.setDirection(DcMotor.Direction.REVERSE);
        rightback.setDirection(DcMotor.Direction.REVERSE);

        double speed = 0.4;

        waitOneFullHardwareCycle();
        waitForStart();

        leftfront.setPower(speed);
        leftback.setPower(speed);
        rightfront.setPower(speed);
        rightback.setPower(speed);
        sleep(2500);

        leftfront.setPower(-speed);
        leftback.setPower(-speed);
        rightfront.setPower(-speed);
        rightback.setPower(-speed);
        sleep(2500);

        leftfront.setPower(0);
        leftback.setPower(0);
        rightfront.setPower(0);
        rightback.setPower(0);
    }
}
